package br.com.longshortstrategy.apicadastro.infra.entrypoint.http.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.longshortstrategy.apicadastro.core.usecase.AtualizarAtivoUseCase;
import br.com.longshortstrategy.apicadastro.core.usecase.BuscarAtivosUseCase;
import br.com.longshortstrategy.apicadastro.core.usecase.BuscarPorCodigoAtivoUseCase;
import br.com.longshortstrategy.apicadastro.core.usecase.CadastrarAtivoUseCase;
import br.com.longshortstrategy.apicadastro.core.usecase.DeletarAtivoUseCase;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.PaginadoAtivoResponse;

/**
 * Controller para acesso aos recursos do ativo Exemplo de acesso: POST
 * http://localhost:8080/api/v1/ativos {endpoint}
 * http://localhost:8080/api/v1/ativos/{id} {endpoint}
 * 
 * @author Fabio Rocha
 *
 */
@RestController
@RequestMapping("/api/v1/ativos")
public class AtivoController extends BaseController {

	private final CadastrarAtivoUseCase cadastrarUseCase;
	private final DeletarAtivoUseCase deletarAtivoUseCase;
	private final BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase;
	private final AtualizarAtivoUseCase atualizarUseCase;
	private final BuscarAtivosUseCase buscarAtivosUseCase;

	public AtivoController(CadastrarAtivoUseCase cadastrarUseCase, DeletarAtivoUseCase deletarAtivoUseCase,
			BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase, AtualizarAtivoUseCase atualizarUseCase,
			BuscarAtivosUseCase buscarAtivosUseCase) {
		this.cadastrarUseCase = cadastrarUseCase;
		this.deletarAtivoUseCase = deletarAtivoUseCase;
		this.buscarPorCodigoAtivoUseCase = buscarPorCodigoAtivoUseCase;
		this.atualizarUseCase = atualizarUseCase;
		this.buscarAtivosUseCase = buscarAtivosUseCase;
	}

	@PostMapping(	consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
					produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> cadastrarAtivo(@RequestBody AtivoRequest request) {
		log.info("Cadastrando ativo: "+ request.getCodigo(), request);
		AtivoResponse response = cadastrarUseCase.executar(request);
		 log.info("Resposta do cadastro: id gerado "+ response.getCodigo() + " id gerado " + response.getId(), response);
		return construirResponse(response);		
	}
	
	
	/**
	 * Realiza a busca do ativo pelo codigo. Exemplo: GET http://localhost:8080/api/v1/ativos/PETR4
	 * @param <T>
	 * @param response
	 * @return
	 */
	@GetMapping("/{codigo}")
	@PreAuthorize("#oauth2.hasScope('read')")
	public ResponseEntity<AtivoResponse> buscarPorCodigo(@PathVariable("codigo") String codigo){
		log.info("Buscando ativo pelo codigo " + codigo);
		AtivoResponse response = buscarPorCodigoAtivoUseCase.executar(codigo);
		log.info("Resposta da busca para o código " + response.getCodigo() + ". ID = " +  response.getId(), response);
		return construirResponse(response);
	}
	
	
	//DELETE http://localhost:8080/api/v1/ativos/{id}
	/***
	 * Deletar um ativo. Exemplo de chamada valida
	 * DELETE http://localhost:8080/api/v1/ativos/{codigo}
	 * @author Fabio Rocha
	 * @param codigo
	 * @return
	 */
	@DeleteMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> deletarAtivo(@PathVariable("codigo") String codigo) {
		log.info("Deletando ativo " + codigo);
		AtivoResponse response = deletarAtivoUseCase.executar(codigo);
		return construirResponse(response);
	}


	

	/**
	 * @author ronaldo.lanhellas
	 * PUT http://localhost:8080/api/v1/ativos/PETR4 
	 * MINHA NOVA DESCRICAO
	 */
	@PutMapping(value = "/{codigo}", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> atualizarAtivo(@RequestBody String descricao,
			@PathVariable("codigo") String codigo) {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(codigo);
		request.setDescricao(descricao);
		AtivoResponse response = atualizarUseCase.executar(request);
		return construirResponse(response);
	}

	/**
	 * Realiza a busca dos ativos. 
	 * Exemplo: 
	 * GET http://localhost:8080/api/v1/ativos?pagina=10
	 * @author Fabio Rocha
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PaginadoAtivoResponse> buscarAtivos(@RequestParam("pagina") Integer pagina) {
		PaginadoAtivoResponse paginadoResponse = buscarAtivosUseCase.executar(pagina);
		return construirResponse(paginadoResponse);
	}
	
	/**
	 * Publico
	 * Exemplo: 
	 * GET http://localhost:8080/api/v1/ativos/public
	 * @author Fabio Rocha
	 */
	@GetMapping("/public")
	public ResponseEntity<String> publicMethod() { 
		return ResponseEntity.ok("its public");
	}
	
}
