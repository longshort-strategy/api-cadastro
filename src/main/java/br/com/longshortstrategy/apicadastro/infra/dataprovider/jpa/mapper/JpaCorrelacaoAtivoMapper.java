package br.com.longshortstrategy.apicadastro.infra.dataprovider.jpa.mapper;

import java.util.Objects;

import br.com.longshortstrategy.apicadastro.core.entity.CorrelacaoAtivo;
import br.com.longshortstrategy.apicadastro.infra.dataprovider.jpa.entity.JpaCorrelacaoAtivoEntity;
 
 
public interface JpaCorrelacaoAtivoMapper {

	static JpaCorrelacaoAtivoEntity toJpa(CorrelacaoAtivo correlacaoAtivo) {
		if (Objects.nonNull(correlacaoAtivo)) {
			JpaCorrelacaoAtivoEntity jpa = new JpaCorrelacaoAtivoEntity();
			jpa.setId(correlacaoAtivo.getId());
			jpa.setDataHoraCriacao(correlacaoAtivo.getDataHoraCriacao());
			jpa.setAtivo1(JpaAtivoMapper.toJpa(correlacaoAtivo.getAtivo1()));
			jpa.setAtivo2(JpaAtivoMapper.toJpa(correlacaoAtivo.getAtivo2()));
			return jpa;
		} else {
			return null;
		}
	}

	static CorrelacaoAtivo toModel(JpaCorrelacaoAtivoEntity jpaEntity) {
		if (Objects.nonNull(jpaEntity)) {
			return new CorrelacaoAtivo(jpaEntity.getId(), JpaAtivoMapper.toModel(jpaEntity.getAtivo1()),
					JpaAtivoMapper.toModel(jpaEntity.getAtivo2()));
		} else {
			return null;
		}
	}

}
