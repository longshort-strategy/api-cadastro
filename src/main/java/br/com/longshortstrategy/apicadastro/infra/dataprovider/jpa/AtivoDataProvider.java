package br.com.longshortstrategy.apicadastro.infra.dataprovider.jpa;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import br.com.longshortstrategy.apicadastro.core.entity.Ativo;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.PaginadoAtivoResponseData;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.longshortstrategy.apicadastro.infra.configuration.mapper.ObjectMapperUtil;
import br.com.longshortstrategy.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.longshortstrategy.apicadastro.infra.dataprovider.jpa.repository.JpaAtivoRepository;
import br.com.longshortstrategy.base.gateway.BuscarPorIdGateway;
import br.com.longshortstrategy.base.gateway.DeletarGateway;
import br.com.longshortstrategy.base.gateway.SalvarGateway;

/***
 * 
 * @author Fabio Rocha
 *
 */

@Repository
public class AtivoDataProvider implements BuscarPorCodigoAtivoGateway, SalvarGateway<Ativo>,
		BuscarPorIdGateway<Ativo, String>, DeletarGateway<String>, BuscarAtivosGateway {

	private final JpaAtivoRepository repository;

	public AtivoDataProvider(JpaAtivoRepository repository) {
		this.repository = repository;
	}

	/**
	 * Busca um ativo por id
	 * 
	 * @author Fabio Rocha
	 * @param id
	 * @return {@Link Ativo}
	 */
	@Override
	public Optional<Ativo> buscarPorId(String id) {
		JpaAtivoEntity jpaAtivoEntity = repository.findById(id).orElse(null);
		if (Objects.nonNull(jpaAtivoEntity)) {
			// ObjectMapper mapper = new ObjectMapper();
			// Ativo ativo = mapper.convertValue(jpaAtivoEntity, Ativo.class);
			// ou
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivoEntity, Ativo.class));
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Salvar um ativo
	 * 
	 * @author Fabio Rocha
	 * @param ativo
	 * @return {@Link Ativo}
	 */
	@Override
	public Ativo salvar(Ativo ativo) {
		JpaAtivoEntity entity = repository.save(ObjectMapperUtil.convertTo(ativo, JpaAtivoEntity.class));
		return ObjectMapperUtil.convertTo(entity, Ativo.class);
	}

	/**
	 * Busca um ativo por código
	 * 
	 * @author Fabio Rocha
	 * @param codigo
	 * @return {@Link Ativo}
	 */
	@Override
	public Optional<Ativo> buscarPorCodigoAtivo(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		if (jpaAtivos.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivos.get(0), Ativo.class));
		}
	}

	/**
	 * Deleta ativo por código
	 * 
	 * @author Fabio Rocha
	 * @param codigo
	 * @return {@Link Ativo}
	 */
	@Override
	public Boolean deletar(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		if (!jpaAtivos.isEmpty()) {
			repository.delete(jpaAtivos.get(0));
			return true;
		}

		return false;
	}

	/**
	 * Busca ativos paginado
	 * 
	 * @author ronaldo.lanhellas
	 */
	@Override
	public PaginadoAtivoResponse buscarAtivos(Integer pagina, Integer qtdPorPagina) {
		Page<JpaAtivoEntity> pageJpaAtivo = repository.findAll(PageRequest.of(pagina, qtdPorPagina));
		PaginadoAtivoResponse response = new PaginadoAtivoResponse();
		response.setPaginaAtual(pagina);
		response.setQtdRegistrosDaPagina(pageJpaAtivo.getNumberOfElements());
		response.setQtdRegistrosTotais(Math.toIntExact(pageJpaAtivo.getTotalElements()));
		pageJpaAtivo.getContent().forEach(c -> response
				.adicionarAtivo(new PaginadoAtivoResponseData(c.getId(), c.getCodigo(), c.getDescricao())));

		return response;
	}

 

	
}
