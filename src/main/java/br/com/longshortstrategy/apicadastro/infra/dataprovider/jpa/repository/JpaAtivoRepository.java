package br.com.longshortstrategy.apicadastro.infra.dataprovider.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;

import br.com.longshortstrategy.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;

public interface JpaAtivoRepository extends JpaRepository<JpaAtivoEntity, String> {
	
	//JPQL = JPA Query Language != HQL (Hibernate query Language)
	//@Query(value = "SELECT ativo FROM JpaAtivoEntity ativo WHERE ativo.codigo = :0")
	List<JpaAtivoEntity> findByCodigo(String codigo);
}
