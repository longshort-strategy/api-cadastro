package br.com.longshortstrategy.apicadastro.infra.entrypoint.http.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.longshortstrategy.apicadastro.core.usecase.CadastrarCorrelacaoAtivoUseCase;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.CorrelacaoAtivoResponse;

@RestController
@RequestMapping("/api/v1/correlacoes")
public class CorrelacaoAtivoController extends BaseController{

	private final CadastrarCorrelacaoAtivoUseCase cadastrarUseCase;

	public CorrelacaoAtivoController(CadastrarCorrelacaoAtivoUseCase cadastrarUseCase) {
		this.cadastrarUseCase = cadastrarUseCase;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<CorrelacaoAtivoResponse> cadastrarCorrelacaoAtivo(@RequestBody CorrelacaoAtivoRequest request) {
		log.info("Cadastrando correlação de ativo {} com ativo {}", request.getAtivo1(), request.getAtivo2());
		CorrelacaoAtivoResponse response = cadastrarUseCase.executar(request);
		log.info("Resposta do cadastro", response);
		return construirResponse(response);
	}

}
