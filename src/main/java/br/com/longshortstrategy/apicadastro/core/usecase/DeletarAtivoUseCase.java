package br.com.longshortstrategy.apicadastro.core.usecase;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.base.usecase.BaseUseCase;

/**
 * Deletar o Ativo
 * 
 * @author Fabio Rocha
 *
 */
public interface DeletarAtivoUseCase extends BaseUseCase<String, AtivoResponse>{
	
}
