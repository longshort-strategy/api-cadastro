package br.com.longshortstrategy.apicadastro.core.usecase.gateway;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;

public interface GerarEventoCorrelacaoAtivoGateway {

	void gerarEvento(CorrelacaoAtivoRequest ativo);

}
