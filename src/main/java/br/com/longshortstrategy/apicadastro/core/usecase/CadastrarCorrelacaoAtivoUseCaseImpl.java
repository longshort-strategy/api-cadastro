package br.com.longshortstrategy.apicadastro.core.usecase;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.longshortstrategy.apicadastro.core.entity.Ativo;
import br.com.longshortstrategy.apicadastro.core.entity.CorrelacaoAtivo;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.CorrelacaoAtivoResponse;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.BuscarCorrelacaoAtivoGateway;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.GerarEventoCorrelacaoAtivoGateway;
import br.com.longshortstrategy.base.dto.response.ListaErroEnum;
import br.com.longshortstrategy.base.dto.response.ResponseDataErro;
import br.com.longshortstrategy.base.gateway.SalvarGateway;
 
@Component
public class CadastrarCorrelacaoAtivoUseCaseImpl implements CadastrarCorrelacaoAtivoUseCase {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SalvarGateway<CorrelacaoAtivo> salvarGateway;
	private final BuscarCorrelacaoAtivoGateway buscarCorrelacaoAtivoGateway;
	private final GerarEventoCorrelacaoAtivoGateway gerarEventoCorrelacaoAtivoGateway;
	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	public CadastrarCorrelacaoAtivoUseCaseImpl(SalvarGateway<CorrelacaoAtivo> salvarGateway,
			BuscarCorrelacaoAtivoGateway buscarCorrelacaoAtivoGateway,
			GerarEventoCorrelacaoAtivoGateway gerarEventoCorrelacaoAtivoGateway,
			BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarCorrelacaoAtivoGateway = buscarCorrelacaoAtivoGateway;
		this.gerarEventoCorrelacaoAtivoGateway = gerarEventoCorrelacaoAtivoGateway;
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	@Override
	public CorrelacaoAtivoResponse executar(CorrelacaoAtivoRequest request) {
		logger.info("Realizando correlação dos ativos {} e {}", request.getAtivo1(), request.getAtivo2());
		CorrelacaoAtivoResponse response = new CorrelacaoAtivoResponse();
		response.setAtivo1(request.getAtivo1());
		response.setAtivo2(request.getAtivo2());

			Ativo ativo1 = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(request.getAtivo1()).orElse(null);
			Ativo ativo2 = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(request.getAtivo2()).orElse(null);

			if (Objects.isNull(ativo1)) {
				response.getResponse().adicionarErro(
						new ResponseDataErro(String.format("Ativo %s não encontrado", request.getAtivo1()),
								ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
			}

			if (Objects.isNull(ativo2)) {
				response.getResponse().adicionarErro(
						new ResponseDataErro(String.format("Ativo %s não encontrado", request.getAtivo2()),
								ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
			}

		if (!response.getResponse().getErros().isEmpty()) {
			return response;
		}

		if (Objects.nonNull(buscarCorrelacaoAtivoGateway.buscar(ativo1, ativo2))) {
			response.getResponse()
					.adicionarErro(new ResponseDataErro(String.format("Ativos já cadastrados para correlação %s e %s",
							request.getAtivo1(), request.getAtivo2()), ListaErroEnum.DUPLICIDADE));
		} else {
			CorrelacaoAtivo correlacaoAtivo = salvarGateway.salvar(new CorrelacaoAtivo(ativo1, ativo2));
			response.setId(correlacaoAtivo.getId());
			gerarEventoCorrelacaoAtivoGateway.gerarEvento(request);
		}

		return response;
	}
}

