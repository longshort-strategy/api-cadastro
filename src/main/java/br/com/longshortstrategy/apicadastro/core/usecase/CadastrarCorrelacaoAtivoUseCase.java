package br.com.longshortstrategy.apicadastro.core.usecase;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.CorrelacaoAtivoResponse;
import br.com.longshortstrategy.base.usecase.BaseUseCase;

public interface CadastrarCorrelacaoAtivoUseCase 
	extends BaseUseCase<CorrelacaoAtivoRequest, CorrelacaoAtivoResponse> {
}
