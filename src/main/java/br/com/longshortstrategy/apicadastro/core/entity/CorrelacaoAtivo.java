package br.com.longshortstrategy.apicadastro.core.entity;

import br.com.longshortstrategy.base.entity.BaseEntity;

public class CorrelacaoAtivo extends BaseEntity {

	private Ativo ativo1;
	private Ativo ativo2;
	
	public CorrelacaoAtivo(Ativo ativo1, Ativo ativo2) {
		this.ativo1 = ativo1;
		this.ativo2 = ativo2;
	}

	
	public CorrelacaoAtivo(String id, Ativo ativo1, Ativo ativo2) {
		this.id = id;
		this.ativo1 = ativo1;
		this.ativo2 = ativo2;
	}

	public Ativo getAtivo1() {
		return ativo1;
	}
	public void setAtivo1(Ativo ativo1) {
		this.ativo1 = ativo1;
	}
	public Ativo getAtivo2() {
		return ativo2;
	}
	public void setAtivo2(Ativo ativo2) {
		this.ativo2 = ativo2;
	}

	

}
