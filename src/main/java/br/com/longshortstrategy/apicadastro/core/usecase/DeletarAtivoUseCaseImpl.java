package br.com.longshortstrategy.apicadastro.core.usecase;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.longshortstrategy.apicadastro.core.entity.Ativo;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.longshortstrategy.base.dto.response.ListaErroEnum;
import br.com.longshortstrategy.base.dto.response.ResponseDataErro;
import br.com.longshortstrategy.base.gateway.DeletarGateway;

/**
 * Deleta o ativo
 * 
 * @author Fabio Rocha
 */
@Service
public class DeletarAtivoUseCaseImpl implements DeletarAtivoUseCase {

	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	private final DeletarGateway<String> deletarGateway;

	public DeletarAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway,
			DeletarGateway<String> deletarGateway) {
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
		this.deletarGateway = deletarGateway;
	}

	/**
	 * executa a deleção do ativo
	 * 
	 * @param id
	 * @return {@link AtivoResponse}
	 **/
	@Override
	public AtivoResponse executar(String codigo) {
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(codigo);

		//TODO: chamar useCase de busca de ativo por codigo e não usar gateway
		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);
		if (opAtivo.isPresent()) {
			boolean deletado = deletarGateway.deletar(codigo);
			if (deletado) {
				response.setId(opAtivo.get().getId());
				response.setDescricao(opAtivo.get().getDescricao());
			}else {
				response.getResponse()
				.adicionarErro(new ResponseDataErro(
						"Ativo de código " + codigo + " não foi deletado. Favor contatar a equipe de suporte.",
						ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR));
			}
		} else {
			response.getResponse().adicionarErro(new ResponseDataErro("Ativo de código " + codigo + " não existe.",
					ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		}

		return response;
	}
}
