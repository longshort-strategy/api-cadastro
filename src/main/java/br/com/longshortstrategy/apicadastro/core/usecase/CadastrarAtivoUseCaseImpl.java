package br.com.longshortstrategy.apicadastro.core.usecase;

import org.springframework.stereotype.Service;

import br.com.longshortstrategy.apicadastro.core.entity.Ativo;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;
import br.com.longshortstrategy.base.dto.response.ListaErroEnum;
import br.com.longshortstrategy.base.dto.response.ResponseDataErro;
import br.com.longshortstrategy.base.gateway.SalvarGateway;
 

/**
 * @author Fabio Rocha
 * Cadastrar ativo 
 */
@Service
 public class CadastrarAtivoUseCaseImpl extends BaseAtivoUseCase implements CadastrarAtivoUseCase {
 
	 private final SalvarGateway<Ativo> salvarGateway;
	 private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	 private final GerarEventoCadastroAtivoGateway gerarEventoCadastroAtivoGateway;
	 
	 public CadastrarAtivoUseCaseImpl(SalvarGateway<Ativo> gateway,
			 						BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway,
			 						GerarEventoCadastroAtivoGateway gerarEventoCadastroAtivoGateway) {
			this.salvarGateway = gateway;
			this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
			this.gerarEventoCadastroAtivoGateway = gerarEventoCadastroAtivoGateway;
		}
	 
/**
 * Executa o cadastro de ativo
 * @param input
 * @return {@link AtivoResponse}
 */
	@Override
	public AtivoResponse executar(AtivoRequest input) {
		AtivoResponse response= new AtivoResponse();
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());
		
		validarCamposObrigatorios(input, response);
		if(response.getResponse().getErros().size() > 0) {
			return response;
		}
		
		if(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo()).isPresent()) {
			response.getResponse().adicionarErro(
					new ResponseDataErro("Ativo já cadastrado com código " + input.getCodigo(), ListaErroEnum.DUPLICIDADE));
		} else
		{
			Ativo ativo = new Ativo();
			ativo.setCodigo(input.getCodigo());
			ativo.setDescricao(input.getDescricao());
			salvarGateway.salvar(ativo);
			gerarEventoCadastroAtivoGateway.gerarEvento(input);
			response.setId(ativo.getId());
		}
		
		
		return response;
	}	
}
