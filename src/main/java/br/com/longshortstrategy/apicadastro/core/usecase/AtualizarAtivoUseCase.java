package br.com.longshortstrategy.apicadastro.core.usecase;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.base.usecase.BaseUseCase;

public interface AtualizarAtivoUseCase extends BaseUseCase<AtivoRequest, AtivoResponse>{
	
}
