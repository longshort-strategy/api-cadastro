package br.com.longshortstrategy.apicadastro.core.usecase;

 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.base.dto.response.ListaErroEnum;
import br.com.longshortstrategy.base.dto.response.ResponseDataErro;
import io.micrometer.core.instrument.util.StringUtils;

public class BaseAtivoUseCase {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected void validarCamposObrigatorios(AtivoRequest input, AtivoResponse response) {
		if(StringUtils.isBlank(input.getCodigo())) {
			String msg = "O campo código é obrigatório";
			logger.error(msg);
			response.getResponse().adicionarErro(
					new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
			
		}
		
		if(StringUtils.isBlank(input.getDescricao())) {
			String msg = "O campo descrição é obrigatório";
			logger.error(msg);
			response.getResponse().adicionarErro(
					new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
			
		}
	}
}
