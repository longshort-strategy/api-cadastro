package br.com.longshortstrategy.apicadastro.core.usecase;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.longshortstrategy.base.usecase.BaseUseCase;

public interface BuscarAtivosUseCase extends BaseUseCase<Integer, PaginadoAtivoResponse> {
}
