package br.com.longshortstrategy.apicadastro.core.usecase.gateway;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoRequest;

/**
 * Gera evento quando ativo é cadastrado
 * @author Fabio Rocha
 *
 */
public interface GerarEventoCadastroAtivoGateway {

	void gerarEvento(AtivoRequest ativo);

}

