package br.com.longshortstrategy.apicadastro.core.entity;

import br.com.longshortstrategy.base.entity.BaseEntity;

/**
 * 
 * @author Fabio Rocha
 * Classe responsável por manter os ativos papéis do nosso domínio 
 * Exemplo: petr4, petr3, vale5
 *  */

public class Ativo extends BaseEntity {

	private String codigo;
	private String descricao;

	public Ativo() {
	}
	
	public Ativo(String id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
