package br.com.longshortstrategy.apicadastro.core.usecase.gateway;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.PaginadoAtivoResponse;

public interface BuscarAtivosGateway {

	PaginadoAtivoResponse buscarAtivos(Integer pagina, Integer qtdPorPagina);

}

