package br.com.longshortstrategy.apicadastro.core.usecase;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.longshortstrategy.apicadastro.core.entity.Ativo;
import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.longshortstrategy.base.dto.response.ListaErroEnum;
import br.com.longshortstrategy.base.dto.response.ResponseDataErro;
import io.micrometer.core.instrument.util.StringUtils;

/**
 * 
 * @author Fabio Rocha Cadastraro Ativo
 */
@Service
public class BuscarPorCodigoAtivoUseCaseImpl implements BuscarPorCodigoAtivoUseCase {

	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	private Logger log = LoggerFactory.getLogger(BuscarPorCodigoAtivoUseCaseImpl.class);

	public BuscarPorCodigoAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	/**
	 * 
	 * executa o cadastro do ativo
	 * 
	 * @param input
	 * @return {@link} CadastrarAtivoResponse}
	 * @author Fabio Rocha
	 * 
	 */
	@Override
	public AtivoResponse executar(String codigo) {
		log.info("Buscando ativo pelo código " + codigo);
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(codigo);

		validarCamposObrigatorios(codigo, response);

		if (!response.getResponse().getErros().isEmpty()) {
			return response;
		}

		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);

		if (opAtivo.isPresent()) {
			log.debug("Ativo encontrado com código " + codigo);
			response.setDescricao(opAtivo.get().getDescricao());
			response.setId(opAtivo.get().getId());
		} else {
			log.debug("Ativo não encontradio com código" + codigo);
			response.getResponse().adicionarErro(new ResponseDataErro("Ativo não encontrado com código " + codigo,
					ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		}

		return response;
	}

	private void validarCamposObrigatorios(String codigo, AtivoResponse response) {
		log.debug("Validando campos obrigatorios");
		log.debug("Codigo: " + codigo);

		if (StringUtils.isBlank(codigo)) {
			response.getResponse().adicionarErro(
					new ResponseDataErro("Campo código é obrigatório", ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
	}
}
