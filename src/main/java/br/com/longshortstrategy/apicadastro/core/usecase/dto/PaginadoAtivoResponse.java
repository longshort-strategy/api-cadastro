package br.com.longshortstrategy.apicadastro.core.usecase.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.longshortstrategy.base.dto.response.BaseResponse;

public class PaginadoAtivoResponse extends BaseResponse {

	private Integer paginaAtual;
	private Integer qtdRegistrosTotais;
	private Integer qtdRegistrosDaPagina;
	private List<PaginadoAtivoResponseData> ativos = new ArrayList<>();

	public Integer getPaginaAtual() {
		return paginaAtual;
	}

	public void setPaginaAtual(Integer paginaAtual) {
		this.paginaAtual = paginaAtual;
	}

	public Integer getQtdRegistrosTotais() {
		return qtdRegistrosTotais;
	}

	public void setQtdRegistrosTotais(Integer qtdRegistrosTotais) {
		this.qtdRegistrosTotais = qtdRegistrosTotais;
	}

	public Integer getQtdRegistrosDaPagina() {
		return qtdRegistrosDaPagina;
	}

	public void setQtdRegistrosDaPagina(Integer qtdRegistrosDaPagina) {
		this.qtdRegistrosDaPagina = qtdRegistrosDaPagina;
	}

	public List<PaginadoAtivoResponseData> getAtivos() {
		return ativos;
	}

	public void adicionarAtivo(PaginadoAtivoResponseData ativoPaginado) {
		this.ativos.add(ativoPaginado);
	}

}

