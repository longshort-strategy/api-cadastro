package br.com.longshortstrategy.apicadastro.core.usecase.gateway;

import br.com.longshortstrategy.apicadastro.core.entity.Ativo;
import br.com.longshortstrategy.apicadastro.core.entity.CorrelacaoAtivo;

public interface BuscarCorrelacaoAtivoGateway {
	CorrelacaoAtivo buscar(Ativo ativo1, Ativo ativo2);
}
