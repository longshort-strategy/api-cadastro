package br.com.longshortstrategy.apicadastro.core.usecase;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.longshortstrategy.base.usecase.BaseUseCase;

public interface BuscarPorCodigoAtivoUseCase extends BaseUseCase<String, AtivoResponse> {

}