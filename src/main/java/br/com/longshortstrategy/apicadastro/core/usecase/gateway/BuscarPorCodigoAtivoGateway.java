package br.com.longshortstrategy.apicadastro.core.usecase.gateway;

import java.util.Optional;

import br.com.longshortstrategy.apicadastro.core.entity.Ativo;

public interface BuscarPorCodigoAtivoGateway  {
	Optional<Ativo> buscarPorCodigoAtivo(String codigo);
}
