package br.com.longshortstrategy.apicadastro.infra.dataprovider.kafka;

import org.springframework.stereotype.Component;

import br.com.longshortstrategy.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.longshortstrategy.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;

@Component
public class ProducerKafkaFake implements GerarEventoCadastroAtivoGateway {

		@Override
		public void gerarEvento(AtivoRequest ativo) {
			// TODO Auto-generated method stub
		}
}
